<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="/css/app.css" type="text/css" rel="stylesheet">

</head>
<body>
<div class="container-fluid">
    <div class="flex-center position-ref full-height mt-1 ml-2">
        <div class="row">

            @yield('contenido')

        </div>
    </div>
</div>
<script src="/js/app.js" type="text/javascript"></script>
</body>
</html>

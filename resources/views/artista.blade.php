@extends('layout')

@section('contenido')
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <img class="h-50 rounded-circle" src="{{ $artista->images[rand(0, count($artista->images) - 1)]->url }}" alt="Card image cap">
                    <div class="col-8">
                        <h3>{{ $artista->name }}</h3>
                    </div>
                    <table class="table mt-2">
                        <thead>
                        <tr>
                            <th scope="col">Foto</th>
                            <th scope="col">Album</th>
                            <th scope="col">Cancion</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($albums as $item)
                            <tr>
                                <th scope="row" height="50"><img height="50" src="{{ $item->images[rand(0, count($item->images) - 1)]->url }}"></th>
                                <td>{{ $item->name }}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
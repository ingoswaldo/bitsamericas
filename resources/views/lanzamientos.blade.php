@extends('layout')

@section('contenido')
    @foreach($lanzamientos as $lanzamiento)
        <div class="col-3 mb-2">
            <div class="card">
                <img class="card-img-top" src="{{ $lanzamiento->images[rand(0, count($lanzamiento->images) - 1)]->url }}" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{ $lanzamiento->name }}</h5>
                    @foreach($lanzamiento->artists as $item)
                        <a href="/artista/{{ $item->id }}" class="badge badge-primary text-white">{{ $item->name }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
@endsection
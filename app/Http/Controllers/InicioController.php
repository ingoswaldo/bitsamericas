<?php

namespace App\Http\Controllers;

use App\Helpers\Spotify;
use Illuminate\Http\Request;

class InicioController extends Controller
{
    /**
     * @var Spotify $spotify
     */
    protected $spotify;

    /**
     * @var string $access_token
     */
    protected $access_token;

    public function __construct()
    {
        $this->spotify = new Spotify(env('SPOTIFY_CLIENT_ID'), env('SPOTIFY_CLIENT_SECRET'));
        $this->spotify->autorizar();
    }

    public function lanzamientos()
    {
        $datos['lanzamientos'] = $this->spotify->getLanzamientos();
        return view('lanzamientos', $datos);
    }

    public function artista($id)
    {
        $datos['artista'] = $this->spotify->getArtista($id);
        $datos['albums'] = $this->spotify->getAlbumsArtista($id);
        return view('artista',$datos);
    }
}

<?php
/**
 * User: Ing. Oswaldo Montes Severiche
 * Date: 19/09/18
 * Time: 1:43 PM
 */

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class Spotify
{
    public const API_URL = 'https://api.spotify.com/v1/';
    public const AUTHORIZE_URL = 'https://accounts.spotify.com/api/token';

    /**
     * @var string $client_id
     */
    protected $client_id;

    /**
     * @var string $secret_id
     */
    protected $secret_id;

    /**
     * @var Client $client
     */
    protected $client;

    protected $response;

    /**
     * Spotify constructor.
     * @param string $clientId
     * @param string $secretId
     */
    public function __construct($clientId, $secretId)
    {
        $this->client_id = $clientId;
        $this->secret_id = $secretId;
        $this->client = new Client(['base_uri' => static::API_URL]);
    }

    public function autorizar()
    {
        $client = new Client(['base_uri' => static::AUTHORIZE_URL]);

        $response = $client->post('', [
                'headers' => [
                    'Authorization' => 'Basic '. base64_encode($this->client_id. ':' .$this->secret_id),
                ],
                'form_params' => ['grant_type' => 'client_credentials']
            ]);

        $this->response = json_decode($response->getBody()->getContents());
    }

    public function getLanzamientos()
    {
        $response =  $this->client->get('browse/new-releases', [
            'headers' => [
                'Authorization' => 'Bearer '. $this->response->access_token,
            ]
        ]);

        $json = json_decode($response->getBody()->getContents());
        return $json->albums->items;
    }

    public function getArtista($id)
    {
        $response =  $this->client->get('artists/'.$id, [
            'headers' => [
                'Authorization' => 'Bearer '. $this->response->access_token,
            ]
        ]);

        return json_decode($response->getBody()->getContents());
    }

    public function getAlbumsArtista($id)
    {
        $response =  $this->client->get('artists/'.$id.'/albums', [
            'headers' => [
                'Authorization' => 'Bearer '. $this->response->access_token,
            ]
        ]);

        return json_decode($response->getBody()->getContents())->items;
    }
}